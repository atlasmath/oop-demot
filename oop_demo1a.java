import java.util.ArrayList;

public class oop_demo1a {

    public static void main(String []args){
        
        class Esiintyja {
            private String nimi;
            private String ala;
            
        }

        class Tilaisuus {
            private String nimi;
            private String tyyppi;
            private Esiintyja esiintyja;
            private double palkkio;

        }

        class Kayttaja {
            private String nimi;
            private String salasana;
        }

        class Sovellus {
            private ArrayList<Esiintyja> esiintyjat;
            private ArrayList<Tilaisuus> tilaisuudet;
            private ArrayList<Kayttaja> kayttajat;
        }

    }
}