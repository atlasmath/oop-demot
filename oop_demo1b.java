import java.util.ArrayList;

public class oop_demo1b {

    public static void main(String []args){
        
        class Esiintyja {
            private String nimi;
            private String ala;
            
            public Esiintyja(String nimi, String ala) {
                this.nimi = nimi;
                this.ala = ala;
            }
        
            public void setNimi(String nimi) {
                this.nimi = nimi;
            }

            public void setAla(String ala) {
                this.ala = ala;
            }

            public String getNimi() {
                return nimi;
            }

            public String getAla() {
                return ala;
            }
        }

        class Tilaisuus {
            private String nimi;
            private String tyyppi;
            private Esiintyja esiintyja;
            private double palkkio;

            public Tilaisuus (String nimi, String tyyppi) {
                this.nimi = nimi;
                this.tyyppi = tyyppi;
                this.palkkio = 0; 
            }

            public void setNimi(String nimi) {
                this.nimi = nimi;
            }

            public void setTyyppi(String tyyppi) {
                this.tyyppi = tyyppi;
            }

            public void setEsiintyja(Esiintyja esiintyja) {
                this.esiintyja = esiintyja;
            }

            public void setPalkkio(double palkkio) {
                this.palkkio = palkkio;
            }

            public String getNimi() {
                return nimi;
            }

            public String getTyyppi() {
                return tyyppi;
            }
            
            public Esiintyja getEsiintyja() {
                return esiintyja;
            }

            public double getPalkkio() {
                return palkkio;
            }
        }

        class Kayttaja {
            private String nimi;
            private String salasana;

            public Kayttaja(String nimi, String salasana) {
                this.nimi = nimi;
                this.salasana = salasana;
            }

            public void setNimi(String nimi) {
                this.nimi = nimi;
            }

            public void setSalasana(String salasana) {
                this.salasana = salasana;
            }

            public String getNimi() {
                return nimi;
            }

            public String getSalasana() {
                return salasana;
            }
        }

        class Sovellus {
            private ArrayList<Esiintyja> esiintyjat;
            private ArrayList<Tilaisuus> tilaisuudet;
            private ArrayList<Kayttaja> kayttajat;

            public Sovellus() {
            }
            
            public void lisaaEsiintyja(Esiintyja e) {
                esiintyjat.add(e);
            }

            public void lisaaTilaisuus(Tilaisuus t) {
                tilaisuudet.add(t);
            }

            public void lisaaKayttaja(Kayttaja k) {
                kayttajat.add(k);
            }

            public ArrayList<Esiintyja> getEsiintyjat() {
                return esiintyjat;
            }

            public ArrayList<Tilaisuus> getTilaisuudet() {
                return tilaisuudet;
            }

            public ArrayList<Kayttaja> getKayttajat() {
                return kayttajat;
            }
        }

    }
}